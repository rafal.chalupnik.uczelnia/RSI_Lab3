﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using WcfServiceLibrary;

namespace WcfServiceHost
{
    internal static class Program
    {
        private static void Main(string[] _args)
        {
            var uri = new Uri("http://localhost:10001/WcfService");
            var serviceHost = new ServiceHost(typeof(Calculator), uri);

            try
            {
                var binding = new WSHttpBinding();
                serviceHost.AddServiceEndpoint(typeof(ICalculator), binding, "endpoint");
                var serviceMetadataBehavior = new ServiceMetadataBehavior {HttpGetEnabled = true};
                serviceHost.Description.Behaviors.Add(serviceMetadataBehavior);

                serviceHost.Open();
                Console.WriteLine("WCF service started!");
                Console.WriteLine("Press Enter to exit.");
                Console.ReadLine();
                serviceHost.Close();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine($"Exception occured: {exception.Message}");
                Console.WriteLine(exception.StackTrace);
                serviceHost.Abort();
                Console.ReadLine();
            }
        }
    }
}
