﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using WcfServiceLibrary;

namespace WcfServiceMultiHost
{
    internal class Program
    {
        private static void PrintContractDescription()
        {
            var contractDescription = ContractDescription.GetContract(typeof(ICalculator));
            Console.WriteLine("Contract information:");
            Console.WriteLine($"\tType: {contractDescription.ContractType}");
            Console.WriteLine($"\tName: {contractDescription.Name}");
            Console.WriteLine("\tOperations:");
            foreach (var description in contractDescription.Operations)
                Console.WriteLine($"\t\t{description.Name}");
        }

        private static void PrintEndpoints(params ServiceEndpoint[] _endpoints)
        {
            foreach (var endpoint in _endpoints)
            {
                Console.WriteLine($"\nService endpoint {endpoint.Name}");
                Console.WriteLine($"Binding: {endpoint.Binding}");
                Console.WriteLine($"ListenUri: {endpoint.ListenUri}");
            }
        }

        private static void Main(string[] _args)
        {
            var serviceHost = new ServiceHost(typeof(Calculator));
            try
            {
                var endpoint1 = serviceHost.Description.Endpoints.Find(typeof(ICalculator));
                var endpoint2 =
                    serviceHost.Description.Endpoints.Find(new Uri("http://localhost:10001/WcfService/Endpoint2"));
                var endpoint3 =
                    serviceHost.Description.Endpoints.Find(new Uri("http://localhost:20001/WcfService/Endpoint3"));
                var endpoint4Address = new Uri("net.tcp://localhost:30000/WcfService");
                var endpoint4 =
                    serviceHost.AddServiceEndpoint(typeof(ICalculator), new NetTcpBinding(), endpoint4Address);

                Console.WriteLine("\n---> Endpoints:");
                PrintEndpoints(endpoint1, endpoint2, endpoint3, endpoint4);
                
                serviceHost.Open();
                Console.WriteLine("WCF service started!");

                PrintContractDescription();

                Console.WriteLine("Press Enter to exit.");
                Console.ReadLine();
                serviceHost.Close();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine($"Exception occured: {exception.Message}");
                Console.WriteLine(exception.StackTrace);
                serviceHost.Abort();
                Console.ReadLine();
            }
        }
    }
}
