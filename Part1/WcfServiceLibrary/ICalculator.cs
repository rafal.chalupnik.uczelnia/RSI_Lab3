﻿using System.ServiceModel;

namespace WcfServiceLibrary
{
    [ServiceContract]
    public interface ICalculator
    {
        [OperationContract]
        double Add(double _x, double _y);

        [OperationContract]
        double Multiply(double _x, double _y);

        [OperationContract]
        double Subtract(double _x, double _y);

        [OperationContract]
        double Sum(double _n);
    }
}