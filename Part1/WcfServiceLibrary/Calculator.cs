﻿using System;
using System.ServiceModel;

namespace WcfServiceLibrary
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Calculator : ICalculator
    {
        private double sum_ = 0.0;

        private static void Log(string _operation, double _x, double _y, double _result)
        {
            Console.WriteLine($"{_operation}({_x}, {_y}) -> {_result}");
        }

        public double Add(double _x, double _y)
        {
            var result = _x + _y;
            Log("Add", _x, _y, result);
            return result;
        }

        public double Multiply(double _x, double _y)
        {
            var result = _x * _y;
            Log("Multiply", _x, _y, result);
            return result;
        }

        public double Subtract(double _x, double _y)
        {
            var result = _x - _y;
            Log("Subtract", _x, _y, result);
            return result;
        }

        public double Sum(double _n)
        {
            sum_ += _n;
            return sum_;
        }
    }
}