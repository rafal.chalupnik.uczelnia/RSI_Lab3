﻿using System;
using System.Collections.Generic;
using WcfServiceClient.WcfCalculator;

namespace WcfServiceClient
{
    internal class Program
    {
        private static void Main(string[] _args)
        {
            // var client = new CalculatorClient();
            var client1 = new CalculatorClient("WSHttpBinding_ICalculator");
            var client2 = new CalculatorClient("Endpoint2");
            var client3 = new CalculatorClient("Endpoint3");
            var client4 = new CalculatorClient("NetTcpBinding_ICalculator");

            var clients = new List<CalculatorClient>()
            {
                client1,
                client2,
                client3,
                client4
            };

            const double x = 1.0;
            const double y = 2.0;
            const double sum = 1.0;

            foreach (var client in clients)
            {
                Console.WriteLine($"Adding {x} and {y}: {client.Add(x, y)}");
                Console.WriteLine($"Sum {sum}: {client.Sum(sum)}");
                Console.WriteLine($"Subtracting {x} and {y}: {client.Subtract(x, y)}");
                Console.WriteLine($"Sum {sum}: {client.Sum(sum)}");
                Console.WriteLine($"Multiplying {x} and {y}: {client.Multiply(x, y)}");

                client.Close();
                Console.WriteLine("");
            }
            Console.ReadLine();
        }
    }
}