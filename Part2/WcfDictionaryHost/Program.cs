﻿using System;
using System.ServiceModel;
using WcfDictionary;

namespace WcfDictionaryHost
{
    /// <summary>
    /// Klasa główna programu hosta.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Punkt wejścia programu hosta.
        /// </summary>
        /// <param name="_args">Argumenty uruchomienia z konsoli (nieużywane).</param>
        public static void Main(string[] _args)
        {
            var serviceHost = new ServiceHost(typeof(Dictionary));
            try
            {
                serviceHost.Open();
                Console.WriteLine("WCF service started!");

                Console.WriteLine("Press Enter to exit.");
                Console.ReadLine();
                serviceHost.Close();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine($"Exception occured: {exception.Message}");
                Console.WriteLine(exception.StackTrace);
                serviceHost.Abort();
                Console.ReadLine();
            }
        }
    }
}