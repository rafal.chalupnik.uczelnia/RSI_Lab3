﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace WcfDictionary
{
    /// <summary>
    /// Implementacja kontraktu usługi słownika.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Dictionary : IDictionary
    {
        /// <summary>
        /// Pole słownika zawierające wpisy tłumaczenia.
        /// </summary>
        private readonly Dictionary<string, string> dictionary_ = new Dictionary<string, string>();

        /// <summary>
        /// Dodanie wpisu do słownika.
        /// </summary>
        /// <param name="_english">Dodawane wyrażenie w języku angielskim.</param>
        /// <param name="_polish">Dodawane wyrażenie w języku polskim.</param>
        /// <returns>Czy udało się dodać wpis.</returns>
        public bool Add(string _english, string _polish)
        {
            Console.WriteLine($"Wywołano metodę Add({_english}, {_polish}).");

            if (string.IsNullOrEmpty(_english) || string.IsNullOrEmpty(_polish))
                return false;

            if (dictionary_.ContainsKey(_english) || dictionary_.ContainsValue(_polish))
                return false;

            dictionary_.Add(_english, _polish);
            Console.WriteLine($"Dodano wpis: {_english} -> {_polish}");
            return true;
        }

        /// <summary>
        /// Usunięcie wpisu ze słownika.
        /// </summary>
        /// <param name="_phrase">Fraza do usunięcia (w dowolnym języku).</param>
        /// <returns>Czy udało się usunąć wpis.</returns>
        public bool Delete(string _phrase)
        {
            Console.WriteLine($"Wywołano metodę Delete({_phrase}).");
            if (string.IsNullOrEmpty(_phrase))
                return false;

            if (dictionary_.ContainsKey(_phrase))
            {
                dictionary_.Remove(_phrase);
                Console.WriteLine($"Usunięto angielski wpis {_phrase}.");
                return true;
            }
                
            if (dictionary_.ContainsValue(_phrase))
            {
                var key = dictionary_.FirstOrDefault(_entry => _entry.Value == _phrase).Key;
                if (!string.IsNullOrEmpty(key))
                    dictionary_.Remove(key);
                Console.WriteLine($"Usunięto polski wpis {_phrase}.");
                return true;
            }

            return false;
        }

        /// <summary>
        /// Przetłumaczenie podanej frazy na drugi język (z angielskiego na polski i na odwrót).
        /// </summary>
        /// <param name="_phrase">Fraza do przetłumaczenia.</param>
        /// <returns>Przetłumaczona fraza.</returns>
        public string Translate(string _phrase)
        {
            Console.WriteLine($"Wywołano metodę Translate({_phrase}).");
            if (string.IsNullOrEmpty(_phrase))
                return null;

            if (dictionary_.ContainsKey(_phrase))
            {
                var keyValue = dictionary_[_phrase];
                Console.WriteLine($"Przetłumaczono {_phrase} -> {keyValue}.");
                return dictionary_[_phrase];
            }

            if (dictionary_.ContainsValue(_phrase))
            {
                var key = dictionary_.FirstOrDefault(_entry => _entry.Value == _phrase).Key;
                Console.WriteLine($"Przetłumaczono {_phrase} -> {key}.");
                return key;
            }

            return null;
        }
    }
}