﻿using System.ServiceModel;

namespace WcfDictionary
{
    /// <summary>
    /// Kontrakt usługi słownika.
    /// </summary>
    [ServiceContract]
    public interface IDictionary
    {
        /// <summary>
        /// Dodanie wpisu do słownika.
        /// </summary>
        /// <param name="_english">Dodawane wyrażenie w języku angielskim.</param>
        /// <param name="_polish">Dodawane wyrażenie w języku polskim.</param>
        /// <returns>Czy udało się dodać wpis.</returns>
        [OperationContract]
        bool Add(string _english, string _polish);

        /// <summary>
        /// Usunięcie wpisu ze słownika.
        /// </summary>
        /// <param name="_phrase">Fraza do usunięcia (w dowolnym języku).</param>
        /// <returns>Czy udało się usunąć wpis.</returns>
        [OperationContract]
        bool Delete(string _phrase);

        /// <summary>
        /// Przetłumaczenie podanej frazy na drugi język (z angielskiego na polski i na odwrót).
        /// </summary>
        /// <param name="_phrase">Fraza do przetłumaczenia.</param>
        /// <returns>Przetłumaczona fraza.</returns>
        [OperationContract]
        string Translate(string _phrase);
    }
}